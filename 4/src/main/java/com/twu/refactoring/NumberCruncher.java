package com.twu.refactoring;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class NumberCruncher {
    private final int[] numbers;

    public NumberCruncher(int... numbers) {
        this.numbers = numbers;
    }

    public int countEven() {
        return (int) Arrays.stream(numbers).filter(number -> number % 2 == 0).count();
    }

    public int countOdd() {
        return numbers.length - this.countEven();
    }

    public int countPositive() {
        return (int) Arrays.stream(numbers).filter(number -> number >= 0).count();
    }

    public int countNegative() {
        return numbers.length - this.countPositive();
    }
}
