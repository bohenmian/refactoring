package com.twu.refactoring;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NumberCruncherTest {

    private int result;

    @Test
    public void shouldCountEvenNumbers() {
        result = new NumberCruncher(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11).countEven();
        assertThat(result, is(5));
    }

    @Test
    public void shouldCountOddNumbers() {
        result = new NumberCruncher(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11).countOdd();
        assertThat(result, is(6));
    }

    @Test
    public void shouldCountPositiveNumbers() {
        result = new NumberCruncher(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4).countPositive();
        assertThat(result, is(5));
    }

    @Test
    public void shouldCountNegativeNumbers() {
        result = new NumberCruncher(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4).countNegative();
        assertThat(result, is(5));
    }
}
