package com.twu.refactoring;

import java.util.List;

public class Order {
    private String name;
    private String address;
    private List<LineItem> lineItems;

    public Order(String nm, String addr, List<LineItem> lineItems) {
        this.name = nm;
        this.address = addr;
        this.lineItems = lineItems;
    }

    public String getCustomerName() {
        return name;
    }

    public String getCustomerAddress() {
        return address;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }
}
