package com.twu.refactoring;

/**
 * OrderReceipt prints the details of order including customer name, address, description, quantity,
 * price and amount. It also calculates the sales tax @ 10% and prints as part
 * of order. It computes the total order amount (amount of individual lineItems +
 * total sales tax) and prints it.
 */
public class OrderReceipt {
    private Order order;
    private static final Double DEFAULT_RATE = 0.1;
    private static final String HEADER = "======Printing Orders======\n";
    private static final String SALES_TEX = "Sales Tax";
    private static final String TOTAL_AMOUNT = "Total Amount";

    public OrderReceipt(Order order) {
        this.order = order;
    }

    public String printReceipt() {
        return formatLineItem();
    }

    private String formatLineItem() {
        StringBuilder result = new StringBuilder();
        result.append(HEADER);
        result.append(order.getCustomerName()).append(order.getCustomerAddress());
        order.getLineItems().forEach(lineItem ->
                result.append(lineItem.getDescription()).append('\t')
                        .append(lineItem.getPrice()).append('\t')
                        .append(lineItem.getQuantity()).append('\t')
                        .append(lineItem.totalAmount()).append('\n'));
        result.append(SALES_TEX).append('\t')
                .append(getTotalAmount() * DEFAULT_RATE).append(TOTAL_AMOUNT)
                .append('\t').append(getTotalAmount() * 1.1);
        return result.toString();
    }

    private double getTotalAmount() {
        return order.getLineItems().stream().mapToDouble(LineItem::totalAmount).sum();
    }
}
